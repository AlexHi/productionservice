package main

import (
	"gitlab.com/AlexHi/productionService/internal/app"
	"gitlab.com/AlexHi/productionService/internal/config"
	"gitlab.com/AlexHi/productionService/pkg/logger"
	"log"
)

func main() {
	log.Print("config init")
	cfg := config.GetConfig()

	log.Print("logger init")
	l := logger.GetLogger(cfg.AppConfig.LogLevel)

	a, err := app.NewApp(cfg, &l)

	if err != nil {
		l.Fatal(err)
	}
	l.Println("Running App")
	a.Run()
}
