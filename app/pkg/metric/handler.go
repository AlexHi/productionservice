package metric

import (
	"github.com/julienschmidt/httprouter"
	"net/http"
)

const (
	URL = "/api/metrics"
)

type Handler struct {
}

// Register TODO remove httprouter as dependency
func (h *Handler) Register(router *httprouter.Router) {
	router.HandlerFunc(http.MethodGet, URL, h.Heartbeat)
}

//Heartbeat
// @Summary Heartbeat metric
// @Tags Metrics
// @Success 204
// @Failure 400
// @Router /api/metrics [get]
func (h *Handler) Heartbeat(w http.ResponseWriter, req *http.Request) {
	w.WriteHeader(204)
}
